/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var _ = require('lodash');

module.exports = {
    index: function(req, res) {
        User.find().exec(function (err, results) {
            if (err) {
                console.log(err);
            }

            return res.json(results);
        })
    },

    signin: function (req, res) {
        var username = req.body.username;
        var password = req.body.password;

        if (!username || !password) {
            return res.json({error: 'email and password required'});
        }

        User.findOne({username: username}).exec(function (err, user) {
            if (!user) {
                return res.json({error: 'username invalid'});
            }

            User.comparePassword(password, user)
                .then(function (valid) {
                    if (valid) {
                        return res.json({
                            token: JwtService.issue({id: user.id})
                        });
                    }
            }).catch((error) => {
                return res.json({error: 'invalid password'});
            });
        });
    },

	signup: function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        var email = req.body.email;
        var phone = req.body.phone;

        var confirmPassword = req.body.confirmPassword;

        if (password !== confirmPassword) {
            return res.json({error: "passwords don't match"});
        }

        User.create({
            username: username,
            password: password,
            email: email,
            phone: phone
        }).exec(function (err, newUser) {
            if (err) {
                return res.send(500, {
                    error: "user could not be created"
                });
            }

            return res.json({
                token: JwtService.issue({id: newUser.id})
            });
        });
    },

    allhobby: function (req, res) {
        var token = req.query.token;

        JwtService.verify(req.query.token, function (err, decoded) {
            if (err) {
                return res.json({error: err});
            }

            if (decoded) {
                User.find({id: decoded.id})
                .populate('hobbies')
                .exec(function (err, hobbies) {
                    if (err) {
                        return res.json({ error: err });
                    }

                    var responseData = {
                        username: hobbies[0].username,
                        hobbies: hobbies[0].hobbies
                    }

                    return res.json(responseData);
                });
            }
        });
    },

    newhobby: function (req, res) {
        var name = req.body.name;
        var token = req.query.token;

        JwtService.verify(req.query.token, function (err, decoded) {
            if (err) {
                return res.json({error: err});
            }

            if (decoded) {
                Hobby.create({
                    name: name,
                    owner: decoded.id
                }).exec(function (err, hobby) {
                    if (err) {
                        return res.json({error: err});
                    }

                    if (hobby !== undefined) {
                        User.find({id: decoded.id})
                        .populate('hobbies')
                        .exec(function (err, hobbies) {
                            if (err) {
                                return res.json({ error: err });
                            }

                            var options = {
                                emailAddress: hobbies[0].email,
                                firstName: hobbies[0].username
                            };

                            SendMessageService.sendNewHobbyEmail(options, function (error) {
                                if (error) {
                                    console.log(error);
                                }

                                SendMessageService.sendNewHobbySms(hobbies[0].phone);
                                // console.log('email sent');
                            });

                            return res.json({hobbies: hobbies[0].hobbies});
                        });
                    }
                });
            }
        });
    },

    deletehobby: function (req, res) {
        var hobbyId = req.body.hobbyId;
        var token = req.query.token;

        JwtService.verify(req.query.token, function (err, decoded) {
            if (err) {
                return res.json({ error: err });
            }

            if (decoded) {
                Hobby.destroy({
                    id: hobbyId
                }).exec(function (err) {
                    if (err) {
                        return res.json({ error: err });
                    }

                    User.find({id: decoded.id})
                    .populate('hobbies')
                    .exec(function (err, hobbies) {
                        if (err) {
                            return res.json({ error: err });
                        }

                        return res.json({hobbies: hobbies[0].hobbies});
                    });
                });
            }
        });
    }
};
