var Mailgun = require('machinepack-mailgun');
var client = require('twilio')('ACc4df0a10f63f2c7040bf98d244f91a5a', '00f0878293d7ff333ccac2ab7d2c9fd4');

module.exports = {
  sendNewHobbyEmail: function (options, done) {
    Mailgun.sendHtmlEmail({
      apiKey: 'key-52d7e9824375956e4d225bce32f39a82',
      domain: 'sandbox19a95adefc2642ad9272a18042f9a758.mailgun.org',
      toEmail: options.emailAddress,
      toName: options.firstName,
      subject: 'Hobbyist: New Hobby!',
      textMessage: 'A new hobby was added from your account `' + options.firstName + '`',
      htmlMessage: 'A new hobby was added from your account `' + options.firstName + '`',
      fromEmail: 'test@hobbyist.com',
      fromName: 'Hobbyist',
    }).exec(function (err) {
      // If an unexpected error occurred...
      if (err) { return done(err); }
      // Otherwise, it worked!
      return done();
    });
  },

  sendNewHobbySms: function (phone) {
    client.messages.create({
      to: '+234' + phone.slice(1),
      from: '+16263250829',
      body: 'New Hobby added!'
    }, function (error, message) {
      if (error) {
        console.log(error);
      } else {
        console.log('Success!');
      }
    });
  },
};