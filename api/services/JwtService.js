var jwt = require('jsonwebtoken');
var jwtSecret = "randomstring";

module.exports = {
    issue: function (payload) {
        token = jwt.sign(payload, jwtSecret);

        return token;
    },

    verify: function (token, callback) {
        return jwt.verify(token, jwtSecret, callback);
    }
}