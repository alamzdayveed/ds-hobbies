import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import Login from './components/login';
import Register from './components/register';
import Dashboard from './components/dashboard';

// const domain = '';
const domain = 'http://localhost:1337';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" render={() => <Dashboard domain={domain} />} />
          <Route path="/signup" render={() => <Register domain={domain} />} />
          <Route path="/signin" render={() => <Login domain={domain} />}/>
        </div>
      </Router>
    );
  }
}

export default App;