import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';

import '../css/login.css';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                username: '',
                email: '',
                phone: '',
                password: '',
                confirmPassword: ''
            },
            redirect: false,
            error: ''
        }

        this.onChange = this.onChange.bind(this);
        this.submitClick = this.submitClick.bind(this);
    }

    onChange(e) {
        var currentFields = this.state.fields;

        currentFields[e.target.name] = e.target.value;

        this.setState({
            fields: currentFields
        });
    }

    submitClick(e) {
        e.preventDefault();

        if (this.state.fields.username.length > 0 
            && this.state.fields.password.length > 0
            && this.state.fields.confirmPassword.length > 0
            && this.state.fields.email.length > 0
            && this.state.fields.phone.length > 0
        ) {
            axios.post(`${this.props.domain}/signup`, {
                username: this.state.fields.username,
                password: this.state.fields.password,
                confirmPassword: this.state.fields.confirmPassword,
                email: this.state.fields.email,
                phone: this.state.fields.phone
            }).then((response) => {
                if (response.data.token) {
                    localStorage.setItem('token', response.data.token);
                    this.setState({
                        redirect: true
                    });
                }
                if (response.data.error) {
                    this.setState({
                        error: response.data.error
                    });
                }
            }).catch((error) => {
                console.error(error);
            });
        } else {
            this.setState({
                error: 'all fields are required'
            });
        }
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to="/" />
            );
        } else {
            return (
                <div className="text-center">
                    <form className="form-signin">
                        <img className="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                        <h1 className="h3 mb-3 font-weight-normal">Please sign up</h1>
                        <label htmlFor="username" className="sr-only">Username</label>
                            <input 
                                type="text" 
                                id="username" 
                                name="username" 
                                value={this.state.fields.username}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Username" 
                                required 
                                autoFocus />
                        <label htmlFor="email" className="sr-only">Email address</label>
                            <input 
                                type="email" 
                                id="email" 
                                name="email"
                                value={this.state.fields.email}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Email address" 
                                required />
                        <label htmlFor="phone" className="sr-only">Phone number</label>
                            <input 
                                type="tel" 
                                id="phone" 
                                name="phone" 
                                value={this.state.fields.phone}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Phone number" 
                                required />
                        <label htmlFor="password" className="sr-only">Password</label>
                            <input 
                                type="password" 
                                id="password" 
                                name="password" 
                                value={this.state.fields.password}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Password" 
                                required />
                        <label htmlFor="confirmPassword" className="sr-only">Password</label>
                            <input 
                                type="password" 
                                id="confirmPassword" 
                                name="confirmPassword" 
                                value={this.state.fields.confirmPassword}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Password Again" 
                                required />

                        {
                            this.state.error.length > 0 ? <div class="alert alert-danger" role="alert">{this.state.error}</div> : ''
                        }
                        
                        <button 
                            className="btn btn-lg btn-primary btn-block" 
                            type="submit"
                        onClick={this.submitClick}>
                                Sign Up
                        </button>
                        <p className="mt-5 mb-3 text-muted"><Link to="/">Already registered? Sign in here!</Link></p>
                    </form>
                </div>
            );
        }
    }
}

export default Register;