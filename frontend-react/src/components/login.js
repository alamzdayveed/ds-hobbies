import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';

import '../css/login.css';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                username: '',
                password: ''
            },
            redirect: false,
            error: ''
        };

        this.onChange = this.onChange.bind(this);
        this.signin = this.signin.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem('token') !== null) {
            this.setState({
                redirect: true
            });
        };
    }

    onChange(e) {
        var currentFields = this.state.fields;

        currentFields[e.target.name] = e.target.value;

        this.setState({
            fields: currentFields
        });
    }

    signin(e) {
        e.preventDefault();

        if (this.state.fields.username.length > 0 && this.state.fields.password.length > 0) {
            axios.post(`${this.props.domain}/signin`, {
                username: this.state.fields.username,
                password: this.state.fields.password
            }).then((response) => {
                if (response.data.token) {
                    localStorage.setItem('token', response.data.token);
                    this.setState({
                        redirect: true
                    });
                }
                if (response.data.error) {
                    this.setState({
                        error: response.data.error
                    });
                }
            }).catch((error) => {
                console.error(error);
            });
        } else {
            this.setState({
                error: 'username and password required'
            });
        }
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to="/" />
            );
        } else {
            return (
                <div className="text-center">
                    <form className="form-signin">
                        <img className="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                        <label htmlFor="username" className="sr-only">Username</label>
                            <input 
                                type="text" 
                                id="username" 
                                name="username" 
                                value={this.state.fields.username}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Username"
                                required 
                                autoFocus />
                        <label htmlFor="password" className="sr-only">Password</label>
                            <input 
                                type="password" 
                                id="password" 
                                name="password" 
                                value={this.state.fields.password}
                                onChange={this.onChange}
                                className="form-control" 
                                placeholder="Password" 
                                required />

                        {
                            this.state.error.length > 0 ? <div class="alert alert-danger" role="alert">{this.state.error}</div> : ''
                        }
                        
                        <button 
                            className="btn btn-lg btn-primary btn-block"
                            onClick={this.signin}
                            type="submit">
                            Sign in
                        </button>
                        <p className="mt-5 mb-3 text-muted"><Link to="/signup">Not registered? Sign up here!</Link></p>
                    </form>
                </div>
            );
        }
    }
}

export default Login;