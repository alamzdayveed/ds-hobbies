import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import '../css/navbar.css';

class Navbar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        }

        this.signout = this.signout.bind(this);
    }

    signout() {
        localStorage.removeItem('token')

        if (localStorage.getItem('token') === null) {
            this.setState({
                redirect: true
            });
        };
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to="/signin" />
            );
        } else {
            return (
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <Link className="navbar-brand" to="/">Hobbyist</Link>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{this.props.username} <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="" onClick={this.signout}>Logout</a></li>
                                </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            );
        }
    }
}

export default Navbar;