import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';
import axios from 'axios';

import Navbar from './navbar';

class HobbyForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: {
                name: ''
            }
        }

        this.create = this.create.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    create(e) {
        e.preventDefault();

        this.props.onCreate(this.state.fields.name);
        var currentFields = this.state.fields;

        currentFields.name = '';

        this.setState({
            fields: currentFields
        });
    }

    onChange(e) {
        var currentFields = this.state.fields;

        currentFields[e.target.name] = e.target.value;

        this.setState({
            fields: currentFields
        });
    }

    render() {
        return (
            <div style={{marginBottom: '15px'}}>
                <form className="form-inline">
                    <div className="form-group" style={{marginRight: '10px'}}>
                        <input type="text" className="form-control" id="name" name="name" value={this.state.fields.name} onChange={this.onChange} placeholder="Hobby Name" autoFocus />
                    </div>
                    <button type="submit" onClick={this.create} className="btn btn-primary"><i className="fa fa-plus"></i></button>
                </form>
            </div>
        );
    }
}

class Hobby extends Component {
    constructor(props) {
        super(props);

        this.delete = this.delete.bind(this);
    }

    delete(e) {
        e.preventDefault();

        this.props.onDelete(this.props.hobby.id);
    }

    render() {
        return (
            <div className="col-md-4">  
                <div className="well">
                    <div className="row">
                        <div className="col-md-10">
                            <label htmlFor="name">{this.props.hobby.name}</label>
                        </div>
                        <div className="col-md-2">
                            <button 
                                type="submit" 
                                onClick={this.delete}
                                className="btn btn-danger mb-2">
                                    <i className="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class HobbyList extends Component {
    render() {
        return (
            <div>
                {
                    this.props.hobbies.map((hobby) => {
                        return (
                            <Hobby 
                                key={hobby.id} 
                                hobby={hobby}
                                onDelete={this.props.onDelete} />
                        )
                    })
                }
            </div>
        );
    }
}

class HobbyDisplay extends Component {
    render() {
        return (
            <div className="text-center">
                <HobbyForm
                    onCreate={this.props.onCreate} />
                <HobbyList 
                    hobbies={this.props.hobbies}
                    onDelete={this.props.onDelete} />
            </div>
        );
    }
}

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            username: '',
            hobbies: []
        }

        this.handleDeleteHobby = this.handleDeleteHobby.bind(this);
        this.handleCreateHobby = this.handleCreateHobby.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem('token') === null) {
            this.setState({
                redirect: true
            });
        };

        axios.get(`${this.props.domain}/users/hobbies?token=${localStorage.getItem('token')}`)
            .then((response) => {
                if (response.data.username && response.data.hobbies) {
                    this.setState({
                        username: response.data.username,
                        hobbies: response.data.hobbies
                    });
                }
            }).catch((error) => {
                console.error(error);
            });
        ;
    }

    handleDeleteHobby(hobbyId) {
        axios.post(`${this.props.domain}/users/hobbies/delete?token=${localStorage.getItem('token')}`, {
            hobbyId: hobbyId
        })
            .then((response) => {
                if (response.data.hobbies.length > 0) {
                    this.setState({
                        hobbies: response.data.hobbies
                    });
                } else {
                    this.setState({
                        hobbies: []
                    });
                }
            }).catch((error) => {
                console.error(error);
            });    
    }

    handleCreateHobby(hobbyName) {
        axios.post(`${this.props.domain}/users/hobbies?token=${localStorage.getItem('token')}`, {
            name: hobbyName
        })
            .then((response) => {
                if (response.data.hobbies.length > 0) {
                    this.setState({
                        hobbies: response.data.hobbies
                    });
                } else {
                    this.setState({
                        hobbies: []
                    });
                }
            }).catch((error) => {
                console.error(error);
            });
    }

    render() {
        if (this.state.redirect) {
            return (
                <Redirect to="/signin" />
            );
        } else {
            return (
                <div className="container">
                    <Navbar username={this.state.username} />

                    <HobbyDisplay 
                        hobbies={this.state.hobbies}
                        onDelete={this.handleDeleteHobby}
                        onCreate={this.handleCreateHobby}
                            />
                </div>
            );
        }
    }
}

export default Dashboard;